package bank.factorymethod;

import bank.rules.*;

import java.util.ArrayList;
import java.util.List;

public class FabricaReglas {

    public static ReglaDeNegocio generateRule(String ruleName){

        if(ruleName.equals("ACTIVA")){

            return new ReglaActiva();

        }

        if(ruleName.equals("MONTO_NEGATIVO")){

            return new ReglaMontoNegativo();

        }

        if(ruleName.equals("MONTO_CERO")){

            return new ReglaMontoCero();

        }

        if(ruleName.equals("CHIP")){

            return new ReglaChip();
        }

        throw new RuntimeException("Regla no encontrada");

    }

    public static List<ReglaDeNegocio> generarConfiguracion(String configuracion){

        List<ReglaDeNegocio> reglasEfectivo = new ArrayList<>();

        if (configuracion.equals("CREDITO")){

            reglasEfectivo.add(FabricaReglas.generateRule("MONTO_CERO"));

        }

        if (configuracion.equals("TRANSFERENCIA")){

            reglasEfectivo.add(FabricaReglas.generateRule("MONTO_CERO"));
            reglasEfectivo.add(FabricaReglas.generateRule("ACTIVA"));

        }

        if (configuracion.equals("CONSIGNACION")){

            reglasEfectivo.add(FabricaReglas.generateRule("MONTO_CERO"));
            reglasEfectivo.add(FabricaReglas.generateRule("ACTIVA"));
            reglasEfectivo.add(FabricaReglas.generateRule("CHIP"));

        }

        return reglasEfectivo;

    }

}
