package bank.rules;

import bank.Transaccion;

public class ReglaBanda implements ReglaDeNegocio{

    public String validate(Transaccion transaccion){

        if (!transaccion.origen.equals("BANDA")){

            return "Regla violada: No es Banda";

        }

        return "";

    }

}
